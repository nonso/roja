# -*- encoding: utf-8 -*-

from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test import LiveServerTestCase
import sys
from pay import models
from collections import OrderedDict

class NewVisitorTest(LiveServerTestCase):
    
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)
        rate = models.ExchangeRate()
        rate.value = 220
        rate.save()

    def tearDown(self):
        self.browser.quit()
    
    def test_can_start_a_transaction_and_finish_it(self):
        # Ngozi opens the home page and sees the exchange rate
        self.browser.get(self.live_server_url)
        rate = models.latest_rate()
        rate_in_html = self.browser.find_element_by_class_name("exchange-rate")
        # She likes the rate
        # She notices the prominent send button
        pay_button = self.browser.find_element_by_id('send_button')
        # She presses send
        pay_button.send_keys(Keys.ENTER)
        sleep(1)
        amount_url = self.browser.current_url
        # She is presented with a form inviting her to fill an amount
        self.assertRegexpMatches(amount_url, '/pay/amount/$') 
        # She brings her cursor in the amount box and sees
        # a 1000 pounds limit on the amount she can send
        # She puts <1000 pounds and hits send
        amount_box = self.browser.find_element_by_class_name('numberinput')
        amount_box.send_keys(1000)
        submit = self.browser.find_element_by_id('finish')
        submit.send_keys(Keys.ENTER)

        # She gets an error that tells her the choice field is required
        choice_error = self.browser.find_element_by_class_name('help-block')
        sleep(1)
        self.assertIn('This field is required', choice_error.text)

        # She is presented with another form that shows calculations
        # She fills 'Pay by Bank Transfer' and clicks send
        choice = self.browser.find_element_by_id('id_amount-payment_choice_1')
        choice.click()
        submit = self.browser.find_element_by_id('finish')
        submit.send_keys(Keys.ENTER)
        self.browser.implicitly_wait(2)
        self.assertRegexpMatches(self.browser.current_url, 'confirm_amount/$')
        ####
        # She is presented with another form that shows calculations
        # showing amount to send, charge, amount + change and the naira equivalent 
        calculations = self.browser.find_elements_by_class_name(
                                                        'form-control-static')
        expected_values = [
                '1,000.00',     # amount to send 
                '50',           # charge
                '1,050.00',     # charge plus amount in gbp
                '220,000.00',   # amount only; in naira
        ] 
        for calc, expected in zip(calculations, expected_values):
            sleep(1) 
            self.assertIn(expected, calc.text)
        # She sees a message telling her that she's not signed in
        sign_in_check = self.browser.find_element_by_id('sign-in-check')
        self.assertIn("You are not signed in", sign_in_check.text)
        # She confirms and is directed to sign-in page
        self.browser.find_element_by_name('confirm_amount-confirm_amount').click() 
        submit = self.browser.find_element_by_id('finish')
        submit.send_keys(Keys.ENTER)
        sleep(1)
        self.assertIn(
            'login/?next=/pay/%s/' % ('receiver' or 'confirm_account'),
            self.browser.current_url
            ) 
                                  
        # She doesn't have an account so she  clicks signup button
        self.browser.find_element_by_id('signup').click()
        self.assertIn('register/', self.browser.current_url) 
         
        # She fills the form and submits and is presented with 
        register_data = {
            'first_name': 'Ngozi',
            'last_name' : 'Ede',
            'username': 'ede',
            'password': 'ede',
            'email': 'ede@eke.com',
            'phone': '2333344',
        }
        found_value = lambda n: self.browser.find_element_by_name(n)
        for i, j in register_data.iteritems():
            found_value(i).send_keys(j)
        self.browser.find_element_by_id('signup').click()

        registered = self.browser.find_element_by_id('registered')
        self.assertEqual("Fantastic! You're registered", registered.text) 
        self.browser.find_element_by_id('send').click()
        
        # form to fill reciever details     
        self.assertIn('/receiver/', self.browser.current_url)
        receiver_data = [
            ('first_name', 'Emeka'),
            ('last_name', 'Zuka'),
            ('bank_account', '12345678'),
            ('confirm_account', '1234'),
            ('bank_name', 'First Bank'),
            ('bank_location', 'Nsukka'), ## phone and email not required                
        ] 
        # she fills the form with differing bank account and confirm account  
        ## remove underscores from values and make change title
        # she finds that not filling required fields yield errors
        normalize_value = lambda value: ' '.join(value.split('_')).title() 
        for h, (i, j) in enumerate(receiver_data): # to get the indices 
            submit = self.browser.find_element_by_id('finish')
            submit.click()
            sleep(1)
            if h < len(receiver_data) - 1: 
                error = self.browser.find_element_by_class_name(
                                            'alert' or 'alert-block').text
                sleep(1)
                self.assertEqual("%s is required" 
                                % normalize_value(i), error)
            found_value('receiver-' + i).send_keys(j)   # see template
            sleep(1)
        # the error of the bank account and confirm account is shown to her
        submit = self.browser.find_element_by_id('finish')
        submit.click()
        sleep(1)
        error = self.browser.find_element_by_class_name(
                                            'alert' or 'alert-block').text
        error_text = "The Account number and Confirm account number do not match. Please re-confirm."
        self.assertEqual(error, error_text)
        found_value('receiver-confirm_account').clear()
        found_value('receiver-confirm_account').send_keys('12345678')
        submit = self.browser.find_element_by_id('finish')
        submit.click()
        sleep(1)
        self.assertIn('/bank/', self.browser.current_url)
        calculations = self.browser.find_elements_by_class_name(
                                                   'form-control-static')
        expected_values = [
            'You have chosen to pay by Bank Transfer.',
            '1,000.00',     # amount to send 
            '50',           # charge
            '1,050.00',     # charge plus amount in gbp
            '220,000.00',   # amount only; in naira
        ] 
        for calc, expected in zip(calculations, expected_values):
            sleep(1) 
            self.assertIn(expected, calc.text)
        payment_id = self.browser.find_element_by_id('payment_id').text
        sleep(1)
        self.assertRegexpMatches(payment_id, '[A-Z]{3}\d{5}')
        confirm = self.browser.find_element_by_id('id_bank-confirm_transaction') 
        confirm.click()
        submit = self.browser.find_element_by_id('finish')
        submit.click()
        sleep(1)
        self.assertIn('/mypage/', self.browser.current_url) 
        ## We confirm that all her captured data were properly saved
        sender = models.Sender.objects.last()
        self.assertEqual(sender.__unicode__(), 'Ngozi Ede')
        self.assertEqual(sender.phone, '2333344') 
        
        receiver = models.Receiver.objects.last()
        self.assertEqual(receiver.__unicode__(), 'Emeka Zuka')
        self.assertEqual(receiver.bank_name, 'First Bank')
        self.assertEqual(receiver.bank_account, '12345678')
        self.assertEqual(receiver.bank_location, 'Nsukka')

        amount = models.AmountToSend.objects.last()
        self.assertEqual(amount.__unicode__(), '1000')
        self.assertEqual(amount.payment_choice, 'bank')
        self.assertRegexpMatches(amount.native_charge_id, '[A-Z]{3}\d{5}')

        def test_payment_can_initiate_to_finish(self):
            pass             
