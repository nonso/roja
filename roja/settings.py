"""
Django settings for roja project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import os

import dj_database_url


# here() gives us file paths from the root of the system to the directory
# holding the current file.

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# Stripe keys
STRIPE_PUBLISHABLE = 'pk_test_FcOeKeRKwRYml6iWmT8P7ovi'
STRIPE_SECRET = 'sk_test_xFAHV6W8Ynzl5Kl1rXoAPfNg'  

# Roja Initial settings
BASIC_CHARGE = 5
INITIAL_RATE = 220
STRIPE_COST = 0.029

CRISPY_TEMPLATE_PACK = 'bootstrap3'

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "nt702wss4o3s#-asq#odnm)f%(_ga5&96pga7df$inh)q=gy!)" # os.environ.get('ROJA_SECRET')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True #bool(os.environ.get('DJANGO_DEBUG'))

TEMPLATE_DEBUG = DEBUG

#EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.zoho.com'
EMAIL_PORT = 465 
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = os.environ.get('ROJA_EMAIL_PASSWORD')
EMAIL_USE_SSL = True 
SERVER_EMAIL = EMAIL_HOST_USER

# Application definition

SITE_ID = 1

DJANGO_APPS = (
    'django.contrib.sites',
    'django_admin_bootstrapped.bootstrap3',
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
)

THIRD_PARTY_APPS = (
    'south',
    'crispy_forms',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'storages',
)

LOCAL_APPS = (
   'pay',

)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

PASSWORD_HASHERS = (
    'roja.hashers.MyPBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


ROOT_URLCONF = 'roja.urls'

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/mypage/'
SOCIALACCOUNT_QUERY_EMAIL = True
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email', 'publish_stream'],
        'METHOD': 'js_sdk'
        }
}

WSGI_APPLICATION = 'roja.wsgi.application'



# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases



SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-gb'

TIME_ZONE = 'Europe/London'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = 'staticfiles'


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

if DEBUG:
    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'roja',
        }
    } 
    STATIC_URL = '/static/'
else:
    AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')

    AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    S3_URL = 'http://%s.s3.amazonaws.com/' %AWS_STORAGE_BUCKET_NAME
    STATIC_URL = S3_URL
    DATABASES = {
        'default': dj_database_url.config(),
    }

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
    'pay.context_processors.exchange_processor',
 )
