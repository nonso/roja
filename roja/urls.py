from django.conf.urls import patterns, include, url
from pay import views
from django.contrib import admin
from django.conf.urls import patterns, url
from pay.views import PaymentWizard, FORMS
from django.contrib.auth.views import logout
from django.contrib.auth.decorators import login_required


payment_wizard = PaymentWizard.as_view(FORMS, url_name='pay_step', done_step_name='finished')

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rojame.views.home', name='home'),
    url(r'^$', views.HomePageView.as_view(), name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^pay/', include('pay.urls', namespace="pay")),
    url(r'^accounts/', include('allauth.urls')),
    #url(r'^accounts/', include('customer.urls')),
    
    url(r'^pay/(?P<step>.+)/$', payment_wizard, name='pay_step'),
    url(r'^pay/$', payment_wizard, name='pay'), 
    url(r'^admin/', include(admin.site.urls)),
    url(r'^mypage/$', views.TableView.as_view(template_name='pay/table-datatable.html'), name='mypage'),
    url(r'^mypage/js/data/', login_required(views.JsonList.as_view()), name='json_list')
)

"""
urlpatterns = patterns('',
    url(r'^$', views.pay, name='pay'),
)
"""
