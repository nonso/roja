"""
Module to make variables available to every template
"""
from django.core.exceptions import ObjectDoesNotExist
from pay import models

def exchange_processor(request):
    """Exchange rate is the latest exchange rate"""
    exchange = models.latest_rate()
    return {'exchange': exchange}
