from django.conf.urls import patterns, url
from pay.views import PaymentWizard, FORMS

payment_wizard = PaymentWizard.as_view(FORMS, url_name='pay_step', done_step_name='finished')





urlpatterns = patterns('',
    url(r'^$', payment_wizard, name='pay'),
    url(r'^(?P<step>.+)/$', payment_wizard, name='pay_step'),
)
