"""
Test views
"""

from django.test import TestCase
from pay import views
from django.core.urlresolvers import reverse
from pay import models
from django.template.loader import render_to_string
import json
from django.contrib.auth.models import User

def fixtures_():
    return ['fixtures/initial.json']


class MyPageFunctionality(TestCase):

    def test_profile_view_requests_for_login(self):
        mypage = self.client.get('/mypage/')
        self.assertRedirects(mypage, 'login/?next=/mypage/')            

class Register(TestCase):
    
    def setUp(self):
        self.register = self.client.post('/register/',
                            {'first_name':'fred',
                             'last_name': 'mike',
                             'username': 'ese',
                             'password': 'pwd',
                             'email': 'ecute@yah.com',
                             'phone': '39383784'
                        })


    # Override setUp register to test lack of data 
    def test_register_returns_errors_to_view(self):
        data = self.client.post('/register/',)   
        self.assertContains(data, 'This field is required.')

    def test_register_uses_correct_templates(self):
        data = self.client.post('/register/')
        template = [i.name for i in data.templates]
        self.assertEqual(template[0], 'pay/register.html') 
    
    def test_register_succeeded_without_errors(self):
        self.assertNotContains(self.register, 'This Field is required.')
        self.assertEqual(self.register.status_code, 200)

    def test_login_redirects_to_mypage_registration(self):
        login = self.client.post('/login/', {'username':'ese', 'password':'pwd'})
        self.assertRedirects(login, '/mypage/')            

class MypageTest(TestCase):
    """
    Test that mypage has all requisite context and the
    controlling view (TableView) works as expected.
    """
    fixtures = fixtures_()

    def setUp(self):
        self.login = self.client.post(
            reverse('login'),
            {'username':'ked', 'password':'password'}
        )

        self.mypage = self.client.get(reverse('json_list'))
        self.json_data = json.loads(self.mypage.content)
        self.user = User.objects.get(username='ked')
        model = models.AmountToSend
        others_amounts = model.objects.exclude(sender=self.user.sender)
        self.others_charge_id = [amt.native_charge_id for amt in others_amounts]
        self.sender_charge_id = [ids['charge_id'] for ids in self.json_data['data']]


    def test_mypage_contains_transactions(self):
        self.assertEqual(self.mypage.status_code, 200)

    def test_mypage_contains_fields(self):
        for key in ['date', 'receiver', 'charge_id',
            'payment', 'transfer_complete', 'amount'
            ]:
            self.assertIn(key , self.json_data['data'][0].keys())
        
    def test_mypage_contains_only_sender_specific_transactions(self):
        """ Uses charge id because it is unique"""
        for charge in self.others_charge_id:
            self.assertNotIn(charge, self.sender_charge_id)
    
    def test_mypage_does_not_contain_others_data(self):
        """ as in above """
        for charge in self.sender_charge_id:
            self.assertNotIn(charge, self.others_charge_id)

class PaymentWizardTest(TestCase):
    
    fixtures = fixtures_()     
    
    def test_url_pay_always_maps_to_pay_amount(self):
        """
        Main url for payment is '/pay/' but must redirect first to
        '/pay/amount/'
        """
        pay = self.client.get('/pay/')
        self.assertRedirects(pay, '/pay/amount/')
    
    def test_paymentwizard_has_correct_form_initial(self):
        """Check if the get_form_initial does its job
        or does it need to be done away with.
        """
        pay = self.client.get('/pay/amount/')
        initial = pay.context['form'].initial.keys()
        for key in ['sender', 'rate', 'receiver']:
            self.assertIn(key, initial)

    def test_templates_used(self):
    
        page = self.client.get('/pay/amount/')
        self.assertTemplateUsed(page, 'pay/amount.html')

    def test_management_form_in_request_to_pay(self):
        """
        The management form should be in response when wizard starts from
        '/pay'.
        """
        page = self.client.get('/pay/', follow=True)
        is_mgt_form = bool(page.context['wizard'].get('management_form', None))
        self.assertTrue(is_mgt_form)

    def test_management_form_not_in_direct_request(self):
        """
        The management form should not exist when the url corresponding
        to a given step is directly requested outwith the wizard flow.
        """
        page = self.client.get('/pay/confirm_amount/')
        is_mgt_form = bool(page.context['wizard'].get('management_form', None))
        self.assertIn('management_form', page.context['wizard'])

    def test_pay_receiver_redirects_to_login(self):
        """
        Takes care of a bug that causes an error if 'pay/receiver' is
        requested directly instead of within the payment wizard flow.
        """
        page = self.client.get('/pay/receiver/')
        self.assertRedirects(page, '/login/?next=/pay/receiver/')

        
