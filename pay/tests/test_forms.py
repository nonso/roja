from pay import forms
from pay import views
from pay import models
from django.test import TestCase
from django.template.loader import render_to_string
from django.template.base import Template

def fixtures_():
    return ['fixtures/initial.json']


class CardPaymentFormTest(TestCase):
    
    fixtures = fixtures_()
    
    def setUp(self):
        pass 

    def test_HtmlModifier_returns_template_object(self):
        temp = forms.HtmlModifier('pay/form_templates/required.html')
        template = temp.get_template()
        self.assertTrue(
            isinstance(template, Template), 
            msg="It is %s instead" % type(template)
        )

