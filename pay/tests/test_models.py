from django.test import TestCase
from pay import models
from django.contrib.auth.models import User


class BusinessLogicTest(TestCase):
    def test_random_id(self):
        for i in range(10):
            id = models.random_id()
        self.assertRegexpMatches(id, '[A-Z]{3}\d{5}')

    def test_charge_logic(self):
        amount_to_result = {10:5, 100:5, 101:10, 200:10,201:15, 300:15}
        for amount, result in amount_to_result.iteritems():
            charge = models.charge_logic(amount)
            self.assertEqual(result, charge)

class AmountToSendTest(TestCase):
    """Tests that the methods calculate properly"""

    def setUp(self):
        exchange_rate = models.ExchangeRate()
        exchange_rate.value = 270
        exchange_rate.save()
        self.amount_to_send = models.AmountToSend(
            rate=exchange_rate, amount=1000
        )

    def payment_choice(self, choice):
        
        self.amount_to_send.payment_choice = choice

    def test_amount_card_charge(self):
        self.payment_choice('card')
        self.assertEqual(self.amount_to_send.charge(), 79)

    def test_amount_card_gbp_total(self):
        self.payment_choice('card')
        self.assertEqual(self.amount_to_send.gbp_total(), 1079)

    def test_amount_card_ngn_total(self):
        self.payment_choice('card')
        self.assertEqual(self.amount_to_send.ngn_total(), 270000)

    def test_amount_bank_charge(self):
        self.payment_choice('bank')
        self.assertEqual(self.amount_to_send.charge(), 50)

    def test_amount_bank_gbp_total(self):
        self.payment_choice('bank')
        self.assertEqual(self.amount_to_send.gbp_total(), 1050)

    def test_amount_bank_ngn_total(self):
        self.payment_choice('bank')
        self.assertEqual(self.amount_to_send.ngn_total(), 270000)

class ExchangeRate(TestCase):

    def test_rate(self):
        rate = models.ExchangeRate.objects.latest('date') 
        self.assertEqual(rate.value, 220)

    def test_rate_shows_on_home_page(self):
        home_page = self.client.get('/')
        self.assertContains(home_page, '210')
