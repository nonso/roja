#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

"""

from django.http import HttpResponseRedirect, HttpResponse
import stripe
from django.shortcuts import render, render_to_response
import roja.settings as settings
from django.contrib.formtools.wizard.views import NamedUrlSessionWizardView
from pay import forms, models
from pay.models import random_id, ExchangeRate
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login, logout
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.contrib.auth.views import login
from django.template.context import RequestContext
from django.views.generic.list import ListView
import json

from django.db.models.signals import post_save
from django.dispatch import receiver

from django_datatables_view.base_datatable_view import BaseDatatableView

RATE = models.latest_rate()

FORMS = [
    ("amount", forms.AmountToSendForm),
    ("confirm_amount", forms.ConfirmAmountForm),
    ("receiver", forms.ReceiverForm),
    ("card", forms.CardPaymentForm),
    ("bank", forms.BankPaymentForm),
]

TEMPLATES = {
    "amount": "pay/amount.html",
    "confirm_amount": "pay/amount_calculated.html",
    "receiver": "pay/receiver.html",
    "card": "pay/card_form.html",
    "bank": "pay/bank_success.html",
}

class TableView(ListView):
    """
    This view populates the sender's home page with
    his/her transactions and their promotional status
    """

    model = models.AmountToSend

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        """
        Used to force login for this view
        """
        return super(TableView, self).dispatch(*args, **kwargs)
    

class JsonList(TemplateView):

    model = models.AmountToSend

    def get_initial_queryset(self):
        user = self.request.user
        return self.model.objects.filter(sender=user.sender)

    def prepare_results(self):
        json_data = []
        qs = self.get_initial_queryset()
        for item in qs:
            charge_id = bool(item.native_charge_id) # Just to be neat
            json_data.append({
                'amount': "%s %s%s" % (u"£", item.amount, '.00'),
                'receiver': item.receiver.__unicode__(),
                'date': item.date_sent.strftime("%m/%d/%Y -  %H:%M"),
                'charge_id': item.native_charge_id if bool(charge_id) else "Paid by Card",
                'payment': "Processed" if item.payment_complete else "Unprocessed",
                'transfer_complete': "Done." if bool(item.transfer_complete) else "Not Yet.",
                'charge': "%s %s%s" % (u'£', item.charge(), '.00')
            })
        ready_json = {"data": json_data}
        return ready_json         
     
    def get(self, request, *args, **kwargs):
        data = self.prepare_results()
        return HttpResponse(
            json.dumps(data),
            content_type="application/json"
        )

class HomePageView(TemplateView):
    template_name = "pay/index.html"


def pay_by_card(wizard):
    """Return true if user opts to pay by credit card"""
    # Get cleaned data from payment step
    cleaned_data = wizard.get_cleaned_data_for_step('amount') or {'method': 'none'}
    # Return the value of payment_choice
    return cleaned_data.get('payment_choice') == 'card'

def pay_by_bank_transfer(wizard):
    """Return true if user opts to pay by bank transfer"""
    # Get cleaned data from payment step
    cleaned_data = wizard.get_cleaned_data_for_step('amount') or {'method': 'none'}
    # Return the value of payment_choice
    return cleaned_data.get('payment_choice') == 'bank'

def login_user(request):
    "Return Django default login"
    return login(
        request, template_name='pay/login.html',
        authentication_form=forms.LoginForm
    )

def logout_view(request):
    logout(request)
    return render(request, 'pay/logged_out.html')

def register(request):
    """
    Register new user. Uses two forms
    """
    context = RequestContext(request)
    registered = False
    if request.method == 'POST':
        user_form = forms.UserForm(data=request.POST)
        sender_form = forms.SenderForm(data=request.POST)
        if user_form.is_valid() and sender_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            sender = sender_form.save(commit=False)
            sender.user = user
            sender.save()
            #required for the next line to work or will get error
            # "'User' object has no attribute 'backend'
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            auth_login(request, user)
            registered = True
    else:
        user_form = forms.UserForm()
        sender_form = forms.SenderForm()
    return render_to_response(
        'pay/register.html', {
            'user_form': user_form,
            'sender_form': sender_form,
            'registered': registered
        },
        context
    )
    
class PaymentWizard(NamedUrlSessionWizardView):
    
    condition_dict = {'card': pay_by_card, 'bank': pay_by_bank_transfer}
    
    def get_form_initial(self,step):
        unbound_sender = models.Sender()
        unbound_receiver = models.Receiver()
        if step == 'amount':
            self.initial_dict['amount'] = {
                'sender' : unbound_sender,
                'rate' : RATE,
                'receiver' : unbound_receiver,
            }
        if step == 'receiver':
            try:
                user = self.request.user #user signed in at this time
                sender = user.sender #using the one-to-one relationship
                self.initial_dict['receiver'] = {'sender' : sender}
            except AttributeError:
                pass
        return self.initial_dict.get(step, {})
    
    
    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def get_context_data(self, form, **kwargs):
        """
        This modification allows required data to be updated for the 
        steps confirm_amount, bank and card.
        """
        context = super(PaymentWizard, self).get_context_data(form=form, **kwargs)
        cleaned_data = super(PaymentWizard, self).get_cleaned_data_for_step('amount') or {'method': 'none'}
        
        payment_choice = cleaned_data.get('payment_choice')
        
        if cleaned_data.get('amount'):
            amount_to_send = forms.AmountToSendForm(cleaned_data)
            amount_sent = amount_to_send.save(commit=False)
            amount_sent.rate = RATE
            amount = amount_sent.amount
            if self.steps.current == 'confirm_amount' or 'bank' or 'card' and not None:
                updates = ({'input_amount': amount},
                {'payment_choice': payment_choice},           
                {'charge': amount_sent.charge},
                {'GBP_total': amount_sent.gbp_total},
                {'NGN_total': amount_sent.ngn_total},
                {'payment_id' : random_id()},
                )
                for update in updates:
                    context.update(update)
        return context
    
    def dispatch(self, *args, **kwargs):
        """
        Modify the dispatch method so that the login_required decorator can fire
        """
        url = self.request.path_info
    
        if url == u'/pay/%s/' %('receiver' or 'card' or 'bank'):
            @method_decorator(login_required)
            def inner_dispatch(self, *args, **kwargs):
                return super(PaymentWizard, self).dispatch(*args, **kwargs)
            return inner_dispatch(self, *args, **kwargs)               
        response = super(PaymentWizard, self).dispatch(*args, **kwargs) 
        return response

    def done(self, form_list, **kwargs):
        """
        Mostly save everything collected by wizard to database.
        """
        # prepare variables
        user = self.request.user
        sender = user.sender #using the one-to-one relationship
        stripe.api_key = settings.STRIPE_SECRET
        form_data = [form.cleaned_data for form in form_list]
        
        #create instances not yet saved to database (variables incomplete)
        unsaved_amounttosend = forms.AmountToSendForm(form_data[0]).save(commit=False)
        if isinstance(form_data[2]['existing_receiver'], models.Receiver):
            receiver = form_data[2]['existing_receiver']
            unsaved_amounttosend.receiver = receiver
        else:
            unsaved_receiver = forms.ReceiverForm(
                form_data[2],).save(commit=False)
            unsaved_receiver.sender = sender
            receiver = unsaved_receiver.save()
            unsaved_amounttosend.receiver = unsaved_receiver

        # append variables
        unsaved_amounttosend.rate = RATE
        unsaved_amounttosend.sender = sender

        # append variables and actions unique to card and bank
        if unsaved_amounttosend.payment_choice == 'card':
            token = form_data[3]['token']
            amount = unsaved_amounttosend.amount
         
            total_charge_stripe = unsaved_amounttosend.gbp_total() * 100
            
            customer = stripe.Customer.create(
                card= token,
                description= sender
            )
            
            stripe_charge = stripe.Charge.create(
              amount= total_charge_stripe,
              currency="gbp",
              customer=customer.id, # obtained with Stripe.js
              description= 'Total payment and charge for payment to {}'.format(receiver if receiver else unsaved_receiver)
            )
            sender.stripe_id = customer.id
            unsaved_amounttosend.stripe_charge_id = stripe_charge.id
            unsaved_amounttosend.payment_complete = True
            
        if unsaved_amounttosend.payment_choice == 'bank':
            unsaved_amounttosend.native_charge_id = form_data[3]['payment_id']
            unsaved_amounttosend.payment_complete = False

        # save everything else to database

        saved_amounttosend = unsaved_amounttosend.save()
        
        
        #remember to create scenerio where user is not a sender
        
        return HttpResponseRedirect('/mypage/')
