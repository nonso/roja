from django_datatables_view.base_datatable_view import BaseDatatableView

class ListJson(BaseDatatableView):

    model = models.AmountToSend

    order_columns = [
        'amount_sent',
        'receiver',
        'charge',
        'rate',
        'date',
        'paid',
        'ref',
        'transfered'
    ]

    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here 
        # by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        user = self.request.user
        return self.model.objects.filter(sender=user.sender)

    def filter_queryset(self, qs):
        # use request parameters to filter queryset

        # simple example:
        search = self.request.POST.get('search[value]', None)
        if search:
            qs = qs.filter(name__istartswith=search)
        return qs

    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        json_data = []
        for item in qs:
            charge_id = bool(item.native_charge_id) # Just to be neat
            json_data.append([
                item,
                item.receiver,
                item.charge,
                item.rate,
                item.date_sent,
                charge_id if bool(charge_id) else "Paid by Card",
                "Done." if bool(item.transfer_complete) else "Not Yet.",
            ])
        return json_data         


