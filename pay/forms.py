"""
Forms
"""

from django import forms
from django.forms import ModelForm
from pay import models
from django.forms import fields, util
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Field, HTML, Row, Div, ButtonHolder, Submit
from crispy_forms.bootstrap import PrependedAppendedText, AppendedText, TabHolder, Tab
from django.core.exceptions import NON_FIELD_ERRORS
from django.utils.dates import MONTHS
import datetime
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from pay.widgets import NoNameSelect, NoNameTextInput
from django.contrib.auth.forms import AuthenticationForm
from django.template.loader import render_to_string, find_template

CSS = {
    'all': (
        'dist/css/bootstrap.css',
        'note/css/animate.css',
        'note/css/font-awesome.min.css',
        'note/css/font.css',
        'note/css/app.css',
    ),
}

JS = (
    'http://code.jquery.com/jquery-2.1.0.min.js',
    'dist/js/bootstrap.js',
)
TEMPLATE_PATH = 'pay/form_templates/'

TEMPLATE_NAMES = (
    'required',
    'wizard_navigation',
    'confirm_login',
    'bank_finish',
    'choose_receiver',
    'stripe_info',
    'card_finish',
)

# Basically to save typing
REQUIRED = 'pay/form_templates/required.html'
WIZARD_NAVIGATION = 'pay/form_templates/wizard_navigation.html'

class HtmlModifier(HTML):
    """
    Inherits from crispy form HTML but modifies it to take html files
    not raw strings. 
    'html' in '__init__' is overloaded with 'template_name' for clarity.
    """
    
    def __init__(self, html):
        super(HtmlModifier, self).__init__(html)
        self.html = html

    def get_template(self): 
        template, dummy = find_template(self.html)
        return template
            
    def render(self, *args, **kwargs):
        super(HtmlModifier, self).render(*args, **kwargs)
        context = args[2]
        template = self.get_template()
        return template.render(context)




class MonospaceForm(forms.Form):
    def addError(self, message):
        self._errors[NON_FIELD_ERRORS] = self.error_class([message])

class CardForm(MonospaceForm):
    token = forms.CharField(required=True, widget=forms.HiddenInput())
    
class CardPaymentForm(CardForm):
    def __init__(self, *args, **kwargs):
        super(CardPaymentForm, self).__init__(*args, **kwargs)
        self.fields['card_cvc'].label = "Card CVC"
        self.fields['card_cvc'].help_text = "Card Verification Code; see rear of card."
        months = [
            (u'%02d' % (m[0]),
            '%s' % (unicode(m[1])))
            for m in sorted(MONTHS.iteritems())
        ]
        self.fields['card_expiry_month'].choices = months
        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.form_id = 'payment-form'
        self.helper.field_class = 'col-xs-6'
        self.helper.label_class = 'col-sm-4'
        self.helper.layout = Layout(
            HTML('<div class="payment-errors text-danger"></div>'),
            Field('token',),
            Field(
                'card_number',
                data_stripe="number",
                placeholder="xxxx xxxx xxxx xxxx",
                id="card_number"
            ),
            Field(
                'card_cvc',
                id="id_card_cvv ",
                data_stripe="cvc",
                placeholder="See reverse of card"
            ),
            Field(
                'card_expiry_month',
                id="expiry_month ",
                data_stripe="exp-month"
            ),
            Field('card_expiry_year', id="expiry_year", data_stripe="exp-year"),
            HtmlModifier('pay/form_templates/stripe_info.html'),
            HtmlModifier('pay/form_templates/card_finish.html')
        )

    months = [
        (u'%02d' % m[0], '%s' % unicode(m[1]))
        for m in sorted(MONTHS.iteritems())
    ]

    today = datetime.date.today()

    card_number = forms.CharField(
        required=False, max_length=20,
        widget=NoNameTextInput(), label="Card Number"
    )

    card_cvc = forms.CharField(
        required=False, max_length=4,
        widget=NoNameTextInput(), label="Card CVC"
    )

    card_expiry_month = forms.ChoiceField(
        required=False,
        widget=NoNameSelect(),
        label="Card Expiry Month",
        choices=months
    )

    card_expiry_year = forms.ChoiceField(
        required=False, widget=NoNameSelect(),
        choices=[(i, i) for i in range(today.year, today.year+12)],
        label="Card Expiry Year"
    )



    class Media:
        css = {
            'all': (
                'dist/css/bootstrap.css',
                'note/css/animate.css',
                'note/css/font-awesome.min.css',
                'note/css/font.css',
                'note/css/app.css',
                'credit_card/css/demo.css',
            ),
            }
        js = (
            'http://code.jquery.com/jquery-2.1.0.min.js',
            'dist/js/bootstrap.js',
            'credit_card/js/demo.js',
            'credit_card/js/jquery.creditCardValidator.js',
        )

        
class ExchangeRateField(forms.DecimalField):
    default_error_messages = {
    'out_of_range': u'Value must be within 248 and 270: Contact Administrator',
    }
    def clean(self, value):
        value = super(ExchangeRateField, self).clean(value)
        if not 248.00 <= value <= 270.00:
            raise util.ValidationError(self.error_messages['out_of_range'])
        return value

class ExchangeRate(forms.Form):
    rate = ExchangeRateField()
        

class LoginForm(AuthenticationForm):
    class Media:
        css = CSS
        js = JS + ('note/js/app.js', 'note/js/app.plugin.js',
            'note/js/slimscroll/jquery.slimscroll.min.js')
        

class UserForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    first_name = forms.CharField(max_length=254, required=True)
    last_name = forms.CharField(max_length=254, required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name','username', 'email', 'password')

        
class SenderForm(ModelForm):
    class Meta:
        model = models.Sender
        fields = ('phone',)
    

class AmountToSendForm(ModelForm):
    """
    The sender, rate, receiver, payment_complete and rate are defined here
    so that the view will not complain during saving.
    """ 
    sender = forms.CharField(widget=forms.HiddenInput, required=False)
    rate = forms.CharField(widget=forms.HiddenInput, required=False)
    receiver = forms.CharField(widget=forms.HiddenInput, required=False)
    payment_complete = forms.BooleanField(widget=forms.HiddenInput, required=False)
    
    payment_choice = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=(
            ('bank', 'Pay By Bank Transfer'),
            ('card', 'Pay By Card')
        )
    )
    
    def __init__(self, *args, **kwargs):
        super(AmountToSendForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.field_class = 'col-xs-5'
        self.helper.label_class = 'col-sm-2'
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
            HTML("""<div class="form-group">
            <div class="col-sm-offset-2">
            <h5 class="form-control-static">How much would you like to send?</h5><br>
            </div>
            </div>"""),
            #'sender',
            'rate',
            'receiver',
            'payment_complete',
            AppendedText(
                'amount',
                '&pound;',
                active=True,
                placeholder='Amount',
            ),
            Field('payment_choice',),
            HtmlModifier('pay/form_templates/required.html'),
            HtmlModifier('pay/form_templates/wizard_navigation.html'),
        )

    class Meta:
        model = models.AmountToSend
        fields = ('amount', 'payment_choice')

    class Media:
        css = CSS
        js = JS

                
        
class ReceiverForm(ModelForm):
    """
    Uses every attribute of the Receiver model but
    distinguishes on the template using manually marked fields
    and validation by Django and html.
    """
    
    bank_account = forms.CharField(label='Account number')
    confirm_account = forms.CharField(label='Repeat account number')
    
    
    class Meta:
        model = models.Receiver
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(ReceiverForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.relevant_fields = [
            'first_name', 'last_name', 'bank_account',
            'confirm_account', 'bank_name', 'bank_location']
        try:
            self.sender = kwargs['initial'].get('sender', None) or sender
            self.fields['existing_receiver'] = forms.ModelChoiceField(
                queryset=models.Receiver.objects.filter(sender=self.sender),
                empty_label='---Choose Receiver---', required=False)
        
        except KeyError:
            pass
        
        for field in self.relevant_fields:
            self.fields[field].required = False
            neat_label_star = field.title().replace('_', ' ') + ' *'
            self.fields[field].label = neat_label_star

        
        self.helper.form_class = 'form-inline'
        self.helper.field_class = 'col-xs-6'
        self.helper.label_class = 'col-sm-4'
        self.helper.layout = Layout(
            TabHolder(
                Tab(
                    'Send to New Receiver',
                    'first_name',
                    'last_name',
                    'bank_account',
                    'confirm_account',
                    'bank_name',
                    'bank_location',
                    'phone',
                    'email',
                    HtmlModifier('pay/form_templates/required.html'),
                    HtmlModifier('pay/form_templates/wizard_navigation.html'),
                ),
                Tab(
                    'Send to Existing Receiver',
                    'existing_receiver',
                    HtmlModifier('pay/form_templates/choose_receiver.html'
                    ),
                    HtmlModifier('pay/form_templates/wizard_navigation.html'),
                )
            )
        )

    def clean(self):
        cleaned_data = super(ReceiverForm, self).clean()
        empty_values = (None, '', [], (), {})

        if isinstance(cleaned_data.get('existing_receiver'), models.Receiver):
            for field in self.relevant_fields:
                if cleaned_data.get(field) not in empty_values:
                    raise forms.ValidationError(
                        '%s%s%s' % (
                            'Please check that you have not filled',
                            ' anything in "Send to New Receiver"',
                            ' if you mean to "Send to Existing Receiver"'
                        )
                    )
        else:
            for field in self.relevant_fields:
                if cleaned_data.get(field) in empty_values:
                    #clean up field for display
                    neat_label = field.title().replace('_', ' ')
                    raise forms.ValidationError(('%s is required') % neat_label)
    
        account_no = cleaned_data.get('bank_account')
        confirm_acc= cleaned_data.get('confirm_account')
    
        if account_no != confirm_acc:
            raise forms.ValidationError(
                '%s%s' %(
                    'The Account number and Confirm account number ',
                    'do not match. Please re-confirm.'
                )
            )
        return cleaned_data

    class Media:
        css = CSS
        js = JS
       
class BankPaymentForm(forms.Form):
    confirm_transaction = forms.BooleanField(required=True)
    payment_id = forms.CharField(max_length=8, widget=forms.HiddenInput)
    
    def __init__(self, *args, **kwargs):
        super(BankPaymentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.field_class = 'col-lg-6'
        self.helper.label_class = 'col-lg-2'
        self.helper.layout = Layout(
            Field(
                'payment_id',
                template="pay/payment_id.html"
            ),
            'confirm_transaction',
            HtmlModifier('pay/form_templates/bank_finish.html'),
        )

    class Media:
        css = CSS
        js = JS
    
class ConfirmAmountForm(forms.Form):
    confirm_amount = forms.BooleanField(required=True, label='Please Confirm')
    
    def __init__(self, *args, **kwargs):
        super(ConfirmAmountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.field_class = 'col-lg-6'
        self.helper.label_class = 'col-lg-2'
        self.helper.layout = Layout(
            'confirm_amount',
            HtmlModifier('pay/form_templates/confirm_login.html'),
            HtmlModifier('pay/form_templates/wizard_navigation.html'),
        ) 
    
    class Media:
        css = CSS
        js = JS
        
