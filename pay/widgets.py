from django.forms.widgets import Select, TextInput
from django.utils.safestring import mark_safe


class NoNameWidget(object):

    def _strip_name_attr(self, widget_string, name):
       return widget_string.replace("name=\"%s\"" % (name,), "data-stripe=\"%s\"" % (name,))


class NoNameTextInput(TextInput, NoNameWidget):

    def render(self, name, *args, **kwargs):
        return mark_safe(self._strip_name_attr(super(NoNameTextInput, self).render(name, *args, **kwargs), name))


class NoNameSelect(Select, NoNameWidget):

    def render(self, name, *args, **kwargs):
        return mark_safe(self._strip_name_attr(super(NoNameSelect, self).render(name, *args, **kwargs), name))