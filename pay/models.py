"""
Contains regualar database elements as per Django,
but also business logic and also the two constants:
BASIC_CHARGE and STRIPE_COST. STRIPE_COST is so labeled
because this is the official stripe charge for their
services. These are considered for inclusion in the
settings. If this is the case the importation should minimize
code refactoring
"""

from django.db import models
from django.contrib.auth.models import User
from random import randrange
from roja import settings


BASIC_CHARGE = settings.BASIC_CHARGE
STRIPE_COST = settings.STRIPE_COST

class ExchangeRate(models.Model):
    "Exchange rate takes value and automatically has a time-stamp"

    value = models.DecimalField(max_digits=5, decimal_places=2)
    date = models.DateTimeField(auto_now=True)
   
    def __unicode__(self):
        return str(self.value)


def latest_rate():
    """
    Creates the initial rate in the database, if it does not exist.
    """
    rate = ExchangeRate.objects.latest('date')
    return rate

def random_id():
    "Generates id for use as reference in bank payments."

    charge_letters = ''
    charge_numbers = ''
    for i in range(3):
        charge_letters += (str(chr(randrange(65,90))))
    for j in range(5):
        charge_numbers += (str(randrange(10)))
    return '%s%s' %(charge_letters, charge_numbers)
    
def charge_logic(amount):
    """
    Generates the basic charge for any transaction.
    The charge is increased by one unit of the constant
    BASIC_CHARGE for every 100 pounds
    """

    if amount <= 100:
        return BASIC_CHARGE * 1
    else:
        if amount%100 == 0:
            return BASIC_CHARGE * (amount/100)
        else:
            return (BASIC_CHARGE * (amount/100)) + BASIC_CHARGE


class IntegerRangeField(models.IntegerField):
    """
    A custom field that subclasses the IntegerField and adds
    min and max values
    """

    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)

#The following allows south to introspect the above custom field
from south.modelsinspector import add_introspection_rules

add_introspection_rules([
    (
        [IntegerRangeField],
        [],
        {
            'min_value': ['min_value', {'min_value': None}],
            'max_value': ['max_value', {'max_value': None}],
        },
    ),
], ['^pay\.models\.IntegerRangeField'])

    
class Sender(models.Model):
    user = models.OneToOneField(User)
    phone = phone = models.CharField(max_length=15)
    house_no = models.CharField(max_length=10, blank=True)
    street_no = models.CharField(max_length=30, blank=True)
    city_town = models.CharField(max_length=30, blank=True)
    postcode = models.CharField(max_length=8, blank=True)
    stripe_id = models.CharField(max_length=100, blank=True)

    def _get_full_name(self):
        "Returns the person's full name."
        return '%s %s' % (self.user.first_name, self.user.last_name)
    full_name = property(_get_full_name)

    def __unicode__(self):
        return str(self.full_name)

class Receiver(models.Model):
    sender = models.ForeignKey(Sender, null=True, blank=True)

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    bank_name = models.CharField(max_length=30)
    bank_account = models.CharField(max_length=15)
    bank_location = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=15, blank=True)
    email = models.EmailField(max_length=254, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def _get_full_name(self):
        "Returns the person's full name."
        return '%s %s' % (self.first_name, self.last_name)
    full_name = property(_get_full_name)

    def __unicode__(self):
        return str(self.full_name)

class AmountToSend(models.Model):
    sender = models.ForeignKey(Sender, null=True, blank=True)
    rate = models.ForeignKey(ExchangeRate, null=True, blank=True)
    receiver = models.ForeignKey(Receiver, null=True, blank=True)

    amount = IntegerRangeField(min_value=0, max_value=1000)

    payment_complete = models.BooleanField(default=False)
    transfer_complete = models.BooleanField(default=False)
    BANK = 'bank'
    CARD = 'card'
    PAYMENT_CHOICES = (
        ('bank', 'Pay by Bank Transfer'),
        ('card', 'Pay by Card'),
        )
    payment_choice = models.CharField(max_length=4, choices=PAYMENT_CHOICES)

    stripe_charge_id = models.CharField(max_length=250, blank=True)
    native_charge_id = models.CharField(max_length=8, blank=True)
    date_sent = models.DateTimeField(auto_now_add=True, editable=False)

    def charge(self):
        if self.payment_choice == 'bank':
            return charge_logic(self.amount)
        if self.payment_choice == 'card':
            return int(charge_logic(self.amount) + (self.amount * STRIPE_COST))

    def gbp_total(self):
        charge_calc = self.charge()
        return self.amount + charge_calc

    def ngn_total(self):
        return int(self.rate.value * self.amount)

    def __unicode__(self):
        return str(self.amount)

