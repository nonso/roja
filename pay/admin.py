from django.contrib import admin
from pay.models import Sender, AmountToSend, Receiver, ExchangeRate

# Register your models here.
admin.site.register(Sender)
admin.site.register(AmountToSend)
admin.site.register(Receiver)
admin.site.register(ExchangeRate)
